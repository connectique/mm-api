package db

import (
	"context"
	"fmt"
	"strings"
	"time"

	"cloud.google.com/go/firestore"
	"github.com/pkg/errors"
	"google.golang.org/api/iterator"
)

// MatchQuerier defines the functions required to fulfill query needs of a match service.
type MatchQuerier interface {
	GetAllSchoolUserIds(ctx context.Context) ([]string, error)
	GetAllUserMatchingProfileIds(ctx context.Context, userID string) ([]string, error)
	GetAllComparisonMatchingProfiles(ctx context.Context, memberType string) (map[string]MatchingProfile, error)
	GetUserMatchingProfile(ctx context.Context, userID, matchingProfileID, memberType string) (MatchingProfile, error)
	GetUserMatchRecords(ctx context.Context, userID string) (map[string]AllMatchRecordInfo, error)
	UpdateMatchRecord(ctx context.Context, mr MatchRecord) error
	CreateMatchRecordAndMaps(ctx context.Context, mr MatchRecord) error
	CreateChatRecords(ctx context.Context, mr MatchRecord) error
}

// New returns a new MatchQuerier with all middleware wired in.
func New(client *firestore.Client) MatchQuerier {
	var mq MatchQuerier
	{
		mq = NewMatchQuerier(client)
	}
	return mq
}

// NewMatchQuerier returns a basic, naive MatchQuerier
func NewMatchQuerier(client *firestore.Client) MatchQuerier {
	return matchQuerier{
		client: client,
	}
}

type matchQuerier struct {
	client *firestore.Client
}

func (q matchQuerier) GetAllSchoolUserIds(ctx context.Context) ([]string, error) {
	ids := []string{}
	docs := q.client.Collection("users").Where("memberType", "==", "school").Documents(ctx)
	for {
		doc, err := docs.Next()
		if err == iterator.Done {
			break
		} else if err != nil {
			return nil, errors.Wrap(err, "")
		}
		ids = append(ids, doc.Ref.ID)
	}
	return ids, nil
}

func (q matchQuerier) GetAllUserMatchingProfileIds(ctx context.Context, userID string) ([]string, error) {
	ids := []string{}
	docs := q.client.Collection("matchingProfiles").Where("userId", "==", userID).Where("complete", "==", true).Documents(ctx)
	for {
		doc, err := docs.Next()
		if err == iterator.Done {
			break
		} else if err != nil {
			return nil, errors.Wrap(err, "")
		}
		ids = append(ids, doc.Ref.ID)
	}
	return ids, nil
}

func (q matchQuerier) GetAllComparisonMatchingProfiles(ctx context.Context, memberType string) (map[string]MatchingProfile, error) {
	docMap := make(map[string]MatchingProfile)
	docs := q.client.Collection("matchingProfiles").Where("memberType", "==", memberType).Where("complete", "==", true).Documents(ctx)
	for {
		doc, err := docs.Next()
		if err == iterator.Done {
			break
		} else if err != nil {
			return nil, errors.Wrap(err, "")
		}
		var mp MatchingProfile
		err = doc.DataTo(&mp)
		if err != nil {
			return nil, errors.Wrap(err, "error converting data to matching profile struct")
		}
		docMap[doc.Ref.ID] = mp
	}
	return docMap, nil
}

// GetUserMP retrieves a single matching profile for a given user.
func (q matchQuerier) GetUserMatchingProfile(ctx context.Context, userID, matchingProfileID, memberType string) (MatchingProfile, error) {
	var mp MatchingProfile
	docs := q.client.Collection("matchingProfiles").
		Where("userId", "==", userID).
		Where("id", "==", matchingProfileID).
		Documents(ctx)
	for {
		doc, err := docs.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return mp, errors.Wrap(err, fmt.Sprintf("error iterating through matchingProfiles for %s, %s", userID, matchingProfileID))
		}
		err = doc.DataTo(&mp)
		if err != nil {
			return mp, errors.Wrap(err, "error converting data to matching profile struct")
		}
		err = doc.DataTo(&mp)
		if err != nil {
			return mp, errors.Wrap(err, fmt.Sprintf("error converting doc to matching profile for %s, %s", userID, matchingProfileID))
		}
	}
	return mp, nil
}

// GetUserMatchRecords retrieves all match records for a given user
func (q matchQuerier) GetUserMatchRecords(ctx context.Context, userID string) (map[string]AllMatchRecordInfo, error) {
	allMatchRecordInfoMap := make(map[string]AllMatchRecordInfo)
	// Get all user match records
	ProfileMatchMapDocs := q.client.Collection("profileMatchMaps").Where("userId", "==", userID).Documents(ctx)
	for {
		// loop through them and then get the matchRecord associated with them
		var ProfileMatchMap ProfileMatchMap
		ProfileMatchMapDoc, err := ProfileMatchMapDocs.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("error iterating through user match records for %s", userID))
		}

		err = ProfileMatchMapDoc.DataTo(&ProfileMatchMap)
		if err != nil {
			return nil, errors.Wrap(err, "error converting data to match record struct")
		}

		// get matchRecord
		matchRecordDoc, err := q.client.Collection("matchRecords").Doc(ProfileMatchMap.MatchRecordID).Get(ctx)
		if err != nil {
			return nil, errors.Wrap(err, fmt.Sprintf("error retrieving matchRecordDoc for id %s", ProfileMatchMap.MatchRecordID))
		}

		var matchRecord MatchRecord
		err = matchRecordDoc.DataTo(&matchRecord)
		if err != nil {
			return nil, errors.Wrap(err, "error converting data to match record struct")
		}

		allMatchRecordInfo := &AllMatchRecordInfo{
			ProfileMatchMap: ProfileMatchMap,
			MatchRecord:     matchRecord,
		}

		if strings.ToUpper(ProfileMatchMap.RecordUserLabel) == "A" {
			allMatchRecordInfoMap[matchRecord.MatchingProfileIDB] = *allMatchRecordInfo
		} else {
			allMatchRecordInfoMap[matchRecord.MatchingProfileIDA] = *allMatchRecordInfo
		}
	}
	return allMatchRecordInfoMap, nil
}

// UpdateUserMatchRecord overwrites an existing single match record for a given user, or creates it if it does not exist
func (q matchQuerier) UpdateMatchRecord(ctx context.Context, mr MatchRecord) error {
	currentTime := time.Now().Unix()

	fmt.Printf("%+v\n", mr)

	// update the matchRecord
	_, err := q.client.Collection("matchRecords").Doc(mr.ID).Set(ctx, mr)
	if err != nil {
		return errors.Wrap(err, "error setting match record to update it")
	}

	// update valid status on ProfileMatchMaps
	pmmADocRef := q.client.Collection("profileMatchMaps").Doc(mr.ProfileAMatchMapID)
	_, err = pmmADocRef.Set(ctx, map[string]interface{}{
		"valid":   mr.Valid,
		"updated": currentTime,
	}, firestore.MergeAll)
	if err != nil {
		return errors.Wrap(err, "error updating valid status of ProfileMatchMapA")
	}

	pmmBDocRef := q.client.Collection("profileMatchMaps").Doc(mr.ProfileBMatchMapID)
	_, err = pmmBDocRef.Set(ctx, map[string]interface{}{
		"valid":   mr.Valid,
		"updated": currentTime,
	}, firestore.MergeAll)
	if err != nil {
		return errors.Wrap(err, "error updating valid status of ProfileMatchMapB")
	}

	return nil
}

func (q matchQuerier) CreateMatchRecordAndMaps(ctx context.Context, matchRecord MatchRecord) error {
	currentTime := time.Now().Unix()

	// create the match record
	mrDocRef := q.client.Collection("matchRecords").NewDoc()
	pmmADocRef := q.client.Collection("profileMatchMaps").NewDoc()
	pmmBDocRef := q.client.Collection("profileMatchMaps").NewDoc()

	matchRecord.ID = mrDocRef.ID
	matchRecord.ProfileAMatchMapID = pmmADocRef.ID
	matchRecord.ProfileBMatchMapID = pmmBDocRef.ID
	_, err := mrDocRef.Set(ctx, matchRecord)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("error creating match record %v", matchRecord))
	}

	pmmA := &ProfileMatchMap{
		MatchingProfileID: matchRecord.MatchingProfileIDA,
		RecordUserLabel:   "A",
		MatchRecordID:     mrDocRef.ID,
		Valid:             true,
		Created:           currentTime,
		Updated:           currentTime,
	}
	pmmB := &ProfileMatchMap{
		MatchingProfileID: matchRecord.MatchingProfileIDB,
		RecordUserLabel:   "B",
		MatchRecordID:     mrDocRef.ID,
		Valid:             true,
		Created:           currentTime,
		Updated:           currentTime,
	}

	_, err = pmmADocRef.Set(ctx, pmmA)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("error creating user match map for user A %v", matchRecord))
	}

	_, err = pmmBDocRef.Set(ctx, pmmB)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("error creating user match map for user B %v", matchRecord))
	}

	return nil
}

func (q matchQuerier) CreateChatRecords(ctx context.Context, mr MatchRecord) error {
	historyRef := q.client.Collection("chatHistories").NewDoc()
	chatRefA := q.client.Collection("chatRecords").NewDoc()
	chatRefB := q.client.Collection("chatRecords").NewDoc()

	history := &ChatHistory{
		UserIDA:  mr.UserIDA,
		UserIDB:  mr.UserIDB,
		Messages: make(map[string]Message),
	}
	_, err := historyRef.Set(ctx, history)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("error setting history record: %v", err))
	}

	chatA := &ChatRecord{
		UserID:        mr.UserIDA,
		ChatPartnerID: mr.UserIDB,
		ChatID:        historyRef.ID,
	}
	_, err = chatRefA.Set(ctx, chatA)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("error setting chat record: %v", err))
	}

	chatB := &ChatRecord{
		UserID:        mr.UserIDB,
		ChatPartnerID: mr.UserIDA,
		ChatID:        historyRef.ID,
	}
	_, err = chatRefB.Set(ctx, chatB)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("error setting chat record: %v", err))
	}

	return nil
}
