package db

// MatchingProfile represents a user's matching profile data
type MatchingProfile struct {
	ID                  string    `json:"id"`
	Name                string    `json:"name"`
	UserID              string    `json:"userId"`
	Role                int64     `json:"role"`
	AgeRanges           []int64   `json:"ageRanges"`
	EducationTypes      []int64   `json:"educationTypes"`
	OrganizationTypes   []int64   `json:"organizationTypes"`
	TrainingTypes       []int64   `json:"trainingTypes"`
	CalendarTypes       []int64   `json:"calendarTypes"`
	CalendarTypesWeight int64     `json:"calendarTypesWeight"`
	LocationTypes       []int64   `json:"locationTypes"`
	LocationTypesWeight int64     `json:"locationTypesWeight"`
	Sizes               []int64   `json:"sizes"`
	SizesWeight         int64     `json:"sizesWeight"`
	Traits              []int64   `json:"traits"`
	TraitsWeight        int64     `json:"traitsWeight"`
	ZipCodes            []ZipCode `json:"zipCodes"`
	ZipCodesWeight      int64     `json:"zipCodesWeight"`
	Created             int64     `json:"created"`
	Updated             int64     `json:"updated"`
	Complete            bool      `json:"complete"`
}

// ZipCode has a the numeric code, and a distance acceptable from that zip code for matching
type ZipCode struct {
	Code     int64 `json:"code"`
	Distance int64 `json:"distance"`
}

// MatchRecord holds metadata about two users who have been matched
// Two copies are made, one for the user and one for the one they are matched with
type MatchRecord struct {
	ID                 string `json:"id"`
	UserIDA            string `json:"userIdA"`
	UserIDB            string `json:"userIdB"`
	MatchingProfileIDA string `json:"matchingProfileIdA"`
	MatchingProfileIDB string `json:"matchingProfileIdB"`
	ProfileAMatchMapID string `json:"profileAMatchMapId"`
	ProfileBMatchMapID string `json:"profileBMatchMapId"`
	Percent            int64  `json:"percent"`
	ConfirmedByUserA   bool   `json:"confirmedByUserA"`
	ConfirmedByUserB   bool   `json:"confirmedByUserB"`
	DeniedByUserA      bool   `json:"deniedByUserA"`
	DeniedByUserB      bool   `json:"deniedByUserB"`
	Valid              bool   `json:"valid"`
	Created            int64  `json:"created"`
	Updated            int64  `json:"updated"`
}

// ProfileMatchMap maps users to a particular match record and which fields inside they represent
type ProfileMatchMap struct {
	MatchingProfileID string `json:"matchingProfileId"`
	MatchRecordID     string `json:"matchRecordId"`
	RecordUserLabel   string `json:"recordUserLabel"`
	Valid             bool   `json:"valid"`
	Created           int64  `json:"created"`
	Updated           int64  `json:"updated"`
}

// AllMatchRecordInfo holds both the record and the user's map
type AllMatchRecordInfo struct {
	ProfileMatchMap ProfileMatchMap
	MatchRecord     MatchRecord
}

// ChatRecord identifies a user, the person they are chatting with, and the id of the chat history doc
type ChatRecord struct {
	UserID        string `json:"userId"`
	ChatPartnerID string `json:"chatPartnerId"`
	ChatID        string `json:"chatId"`
}

// ChatHistory holds an array of message objects in the chat
type ChatHistory struct {
	UserIDA  string             `json:"userIdA"`
	UserIDB  string             `json:"userIdB"`
	Messages map[string]Message `json:"messages"`
}

// Message holds the text of a message in a chat as well as metadata about it
type Message struct {
	UserID    string `json:"userId"`
	Text      string `json:"text"`
	Timestamp int64  `json:"timestamp"`
}
