package db

import (
	"context"
	"os"

	"cloud.google.com/go/firestore"
	"firebase.google.com/go"
	"github.com/pkg/errors"
	"google.golang.org/api/option"
)

// NewClient returns a Cloud Firestore client
func NewClient(ctx context.Context, opt option.ClientOption) (*firestore.Client, error) {
	config := &firebase.Config{
		ProjectID: os.Getenv("PROJECT_ID"),
	}

	app, err := firebase.NewApp(ctx, config, opt)
	if err != nil {
		return nil, errors.Wrap(err, "could not create firebase app")
	}

	client, err := app.Firestore(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "could not create firestore client")
	}

	return client, nil
}
