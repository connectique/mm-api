package service

import (
	"context"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/connectique/mm-api/pkg/match/algorithm"
	"gitlab.com/connectique/mm-api/pkg/match/db"
)

// MatchService defines the functions required to implement the matchorization service.
type MatchService interface {
	RunMatchAlgorithm(ctx context.Context)
}

// New returns a new MatchService with all middleware wired in.
func New(runner algorithm.Runner, querier db.MatchQuerier) MatchService {
	var svc MatchService
	{
		svc = NewMatchService(runner, querier)
	}
	return svc
}

// NewMatchService returns a basic, naive MatchService
func NewMatchService(runner algorithm.Runner, querier db.MatchQuerier) MatchService {
	return matchService{
		AlgorithmRunner: runner,
		Querier:         querier,
	}
}

type matchService struct {
	AlgorithmRunner algorithm.Runner
	Querier         db.MatchQuerier
}

func (s matchService) RunMatchAlgorithm(ctx context.Context) {
	schoolIDs, err := s.Querier.GetAllSchoolUserIds(ctx)
	if err != nil {
		fmt.Println(errors.WithStack(err))
	}
	comparisonMatchingProfiles, err := s.Querier.GetAllComparisonMatchingProfiles(ctx, "educator")
	if err != nil {
		fmt.Println(errors.WithStack(err))
	}

	for _, userID := range schoolIDs {
		matchingProfileIDs, err := s.Querier.GetAllUserMatchingProfileIds(ctx, userID)
		if err != nil {
			fmt.Println(errors.WithStack(err))
		}
		if len(matchingProfileIDs) == 0 {
			continue
		}

		for _, matchingProfileID := range matchingProfileIDs {
			userMatchingProfile, err := s.Querier.GetUserMatchingProfile(ctx, userID, matchingProfileID, "school")
			if err != nil {
				fmt.Println(errors.WithStack(err))
			}

			for otherMatchingProfileID, comparisonMatchingProfile := range comparisonMatchingProfiles {
				currentTime := time.Now().Unix()
				score := s.AlgorithmRunner.GetMatchScore(userMatchingProfile, comparisonMatchingProfile)
				roundScore := int64(0)
				if score != 0 {
					roundScore = int64(score + 1.0)
				}
				// check if pairing is already in match records
				userMatchRecords, err := s.Querier.GetUserMatchRecords(ctx, userID)
				if err != nil {
					fmt.Println(errors.WithStack(err))
				}

				allMatchRecordInfo, present := userMatchRecords[otherMatchingProfileID]
				if !present {
					// if not present, create match record
					valid := false
					if roundScore > 70 {
						valid = true
					}
					mr := &db.MatchRecord{
						Percent:            roundScore,
						Valid:              valid,
						Created:            currentTime,
						Updated:            currentTime,
						UserIDA:            userID,
						MatchingProfileIDA: matchingProfileID,
						MatchingProfileIDB: otherMatchingProfileID,
						UserIDB:            comparisonMatchingProfile.UserID,
					}

					err = s.Querier.CreateMatchRecordAndMaps(ctx, *mr)
					if err != nil {
						fmt.Println(errors.WithStack(err))
					}

					// // create chat
					if valid {
						err = s.Querier.CreateChatRecords(ctx, *mr)
						if err != nil {
							fmt.Println(errors.WithStack(err))
						}
					}
					
					continue
				}

				// if present, update score, if score is below 70, invalidate it if not already done
				allMatchRecordInfo.MatchRecord.Percent = roundScore
				allMatchRecordInfo.MatchRecord.Updated = currentTime
				if score > 70 {
					allMatchRecordInfo.MatchRecord.Valid = true
				} else {
					allMatchRecordInfo.MatchRecord.Valid = false
				}
				err = s.Querier.UpdateMatchRecord(ctx, allMatchRecordInfo.MatchRecord)
				if err != nil {
					fmt.Println(errors.WithStack(err))
				}
				continue
			}
		}
	}
}
