package calculators

import (
	"math"

	"gitlab.com/connectique/mm-api/pkg/match/algorithm/zipcodes"
	"gitlab.com/connectique/mm-api/pkg/match/db"
)

// Calculators has methods for calculuating scores within the algorithm.
type Calculators interface {
	// SomeMatch returns true if two slices share any common values
	SomeMatch(sliceA, sliceB []int64) bool
	// CountNumOfMatches returns the number of common values in two slices
	CountNumOfMatches(sliceA, sliceB []int64) int64
	GetDecimal(dividend, divisor float64) float64
	GetPercentIfSome(sliceA, sliceB []int64) float64
	GetOneIfSome(sliceA, sliceB []int64) float64
	GetZipScore(data map[int64]zipcodes.ZipData, sliceA, sliceB []db.ZipCode) float64
	MatchPercentOneWay(ageScore, ageWgt, edScore, edWgt, orgScore, orgWgt, trainingScore, trainingWgt,
		calScore, calWgt, locScore, locWgt,
		sizeScore, sizeWgt, traitScore, traitWgt, zipScore, zipWgt float64) float64
	MatchPercentMutual(scoreA, scoreB float64) float64
}

// New returns a new set of calculators.
func New() Calculators {
	return calcs{}
}

type calcs struct{}

// SomeMatch returns true if two slices share any common values
func (c calcs) SomeMatch(sliceA, sliceB []int64) bool {
	for _, valueA := range sliceA {
		for _, valueB := range sliceB {
			if valueA == valueB {
				return true
			}
		}
	}
	return false
}

// CountNumOfMatches returns the number of common values in two slices
func (c calcs) CountNumOfMatches(sliceA, sliceB []int64) (num int64) {
	for _, valueA := range sliceA {
		for _, valueB := range sliceB {
			if valueA == valueB {
				num++
			}
		}
	}
	return num
}

// GetDecimal does division on two numbers
func (c calcs) GetDecimal(dividend, divisor float64) float64 {
	return float64(dividend) / float64(divisor)
}

// GetPercentIfSome calls SomeMatch and if there are matches returns a decimal
// of how many matches there are out of the whole
func (c calcs) GetPercentIfSome(sliceA, sliceB []int64) float64 {
	if c.SomeMatch(sliceA, sliceB) {
		count := float64(c.CountNumOfMatches(sliceA, sliceB))
		length := float64(len(sliceA))
		return c.GetDecimal(count, length)
	}
	return 0
}

// GetOneIfsome returns one if there are any matches, otherwise returns 0
func (c calcs) GetOneIfSome(sliceA, sliceB []int64) float64 {
	if c.SomeMatch(sliceA, sliceB) {
		return 1
	}
	return 0
}

func (c calcs) GetZipScore(data map[int64]zipcodes.ZipData, sliceA, sliceB []db.ZipCode) float64 {
	someMatch := false
	for _, valueA := range sliceA {
		for _, valueB := range sliceB {
			dataA := data[valueA.Code]
			dataB := data[valueB.Code]

			dist := zipcodes.CalculateDistance(dataA.Latitude, dataB.Latitude, dataA.Longitude, dataB.Longitude)

			if dist <= float64(valueA.Distance) {
				someMatch = true
			}
		}
	}
	if someMatch {
		return 1
	}
	return 0
}

func (c calcs) MatchPercentOneWay(ageScore, ageWgt, edScore, edWgt, orgScore, orgWgt, trainingScore, trainingWgt,
	calScore, calWgt, locScore, locWgt,
	sizeScore, sizeWgt, traitScore, traitWgt, zipScore, zipWgt float64) float64 {
	ageScore = ageScore * ageWgt
	edScore = edScore * edWgt
	orgScore = orgScore * orgWgt
	trainingScore = trainingScore * trainingWgt
	calScore = calScore * calWgt
	locScore = locScore * locWgt
	sizeScore = sizeScore * sizeWgt
	traitScore = traitScore * traitWgt
	zipScore = zipScore * zipWgt

	score := ageScore + edScore + orgScore + trainingScore + calScore + locScore + sizeScore + traitScore + zipScore
	divisor := ageWgt + edWgt + orgWgt + trainingWgt + calWgt + locWgt + sizeWgt + traitWgt + zipWgt

	return c.GetDecimal(score, divisor)
}

func (c calcs) MatchPercentMutual(scoreA, scoreB float64) float64 {
	return math.Sqrt(scoreA * scoreB)
}
