package zipcodes

import (
	"bytes"
	"encoding/csv"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

// ZipData is a model for zip code data.
type ZipData struct {
	ZipCode    int64
	StateAbbrv string
	Latitude   float64
	Longitude  float64
	City       string
	State      string
}

// CsvHandler reads zip code data from a remote file.
type CsvHandler interface {
	ReadZipDataFromFile(file []byte) (map[int64]ZipData, error)
}

// New returns a new CsvHandler.
func New() CsvHandler {
	return handler{}
}

type handler struct{}

func (h handler) ReadZipDataFromFile(csvFile []byte) (map[int64]ZipData, error) {
	reader := csv.NewReader(bytes.NewReader(csvFile))
	zipMap := make(map[int64]ZipData)
	reader.TrimLeadingSpace = true
	lines, err := reader.ReadAll()
	if err != nil {
		return nil, errors.Wrap(err, "line read error")
	}
	for i, line := range lines {
		if i == 0 {
			continue
		}

		zipCode, err := strconv.ParseInt(line[0], 10, 64)
		if err != nil {
			continue
		}

		stateAbbrv := line[1]

		lat, err := strconv.ParseFloat(strings.TrimSpace(line[2]), 64)
		if err != nil {
			return nil, errors.Wrap(err, "")
		}
		long, err := strconv.ParseFloat(strings.TrimSpace(line[3]), 64)
		if err != nil {
			return nil, errors.Wrap(err, "")
		}
		city := line[4]
		state := line[5]

		zipMap[zipCode] = ZipData{
			ZipCode:    zipCode,
			StateAbbrv: stateAbbrv,
			Latitude:   lat,
			Longitude:  long,
			City:       city,
			State:      state,
		}
	}
	return zipMap, nil
}
