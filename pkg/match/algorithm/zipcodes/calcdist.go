package zipcodes

import "math"

const x = math.Pi / 180

func rad(d float64) float64 {
	return d * x
}

func deg(r float64) float64 {
	return r / x
}

// CalculateDistance gets the distance in miles between two geographic points.
func CalculateDistance(latA, longA, latB, longB float64) float64 {
	distance := math.Sin(rad(latA))*
		math.Sin(rad(latB)) +
		math.Cos(rad(latA))*
			math.Cos(rad(latB))*
			math.Cos(rad(longA-longB))
	return deg(math.Acos(distance)) * float64(69.09)
}
