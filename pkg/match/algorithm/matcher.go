package algorithm

import (
	"gitlab.com/connectique/mm-api/pkg/match/algorithm/calculators"
	"gitlab.com/connectique/mm-api/pkg/match/algorithm/zipcodes"
	"gitlab.com/connectique/mm-api/pkg/match/db"
)

// Runner contains the algorithm code in accessible methods.
type Runner interface {
	GetMatchScore(mpA, mpB db.MatchingProfile) float64
}

// New returns a new runner.
func New(data map[int64]zipcodes.ZipData, calcs calculators.Calculators) Runner {
	return runner{
		ZipData: data,
		Calc:    calcs,
	}
}

type runner struct {
	ZipData map[int64]zipcodes.ZipData
	Calc    calculators.Calculators
}

// GetMatchScore takes two matching profiles and returns a match score.
func (r runner) GetMatchScore(mpA, mpB db.MatchingProfile) float64 {
	ageRangesScore := r.Calc.GetOneIfSome(mpA.AgeRanges, mpB.AgeRanges)
	if ageRangesScore == 0 {
		return 0
	}
	edTypesScore := r.Calc.GetOneIfSome(mpA.EducationTypes, mpB.EducationTypes)
	if edTypesScore == 0 {
		return 0
	}
	orgTypesScore := r.Calc.GetOneIfSome(mpA.OrganizationTypes, mpB.OrganizationTypes)
	if orgTypesScore == 0 {
		return 0
	}
	trainingScore := r.Calc.GetOneIfSome(mpA.TrainingTypes, mpB.TrainingTypes)
	if trainingScore == 0 {
		return 0
	}

	traitScore := r.Calc.GetPercentIfSome(mpA.Traits, mpB.Traits)
	calScore := r.Calc.GetOneIfSome(mpA.CalendarTypes, mpB.CalendarTypes)
	locScore := r.Calc.GetOneIfSome(mpA.LocationTypes, mpB.LocationTypes)
	sizeScore := r.Calc.GetOneIfSome(mpA.Sizes, mpB.Sizes)
	zipScore := r.Calc.GetZipScore(r.ZipData, mpA.ZipCodes, mpB.ZipCodes)

	matchPercentA := r.Calc.MatchPercentOneWay(ageRangesScore, 10, edTypesScore, 10, orgTypesScore, 10,
		trainingScore, 10, calScore, float64(mpA.CalendarTypesWeight), locScore, float64(mpA.LocationTypesWeight),
		sizeScore, float64(mpA.SizesWeight), traitScore, float64(mpA.TraitsWeight), zipScore, 5)

	matchPercentB := r.Calc.MatchPercentOneWay(ageRangesScore, 10, edTypesScore, 10, orgTypesScore, 10,
		trainingScore, 10, calScore, float64(mpB.CalendarTypesWeight), locScore, float64(mpB.LocationTypesWeight),
		sizeScore, float64(mpB.SizesWeight), traitScore, float64(mpB.TraitsWeight), zipScore, 5)

	finalScore := r.Calc.MatchPercentMutual(matchPercentA, matchPercentB)

	return finalScore
}
