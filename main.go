package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"

	"io/ioutil"

	"gitlab.com/connectique/mm-api/pkg/match/algorithm"
	"gitlab.com/connectique/mm-api/pkg/match/algorithm/calculators"
	"gitlab.com/connectique/mm-api/pkg/match/algorithm/zipcodes"
	"gitlab.com/connectique/mm-api/pkg/match/db"
	"gitlab.com/connectique/mm-api/pkg/match/service"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)
	go func() {
		sig := <-signalChannel
		switch sig {
		case os.Interrupt:
			cancel()
			os.Exit(0)
		case syscall.SIGTERM:
			cancel()
			os.Exit(0)
		}
	}()

	log.Println("starting up!")

	// set up storage client and retrive zips
	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile(os.Getenv("STORAGE_CREDENTIAL_PATH")))
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	log.Println("created storage client")
	rc, err := storageClient.Bucket("mm-api-files").Object("zips.csv").NewReader(ctx)
	if err != nil {
		log.Fatalf("Failed to retrieve csv file from bucket: %v", err)
	}
	defer rc.Close()

	zipFile, err := ioutil.ReadAll(rc)
	if err != nil {
		log.Fatalf("Failed to retrieve csv file from bucket: %v", err)
	}
	log.Println("read zipfile")

	// set up csv handler and get csv data in memory
	csvHandler := zipcodes.New()
	if err != nil {
		log.Fatalf("Failed to find csv file: %s", err)
	}
	log.Println("created csvHandler")
	zipData, err := csvHandler.ReadZipDataFromFile(zipFile)
	if err != nil {
		log.Fatalf("Failed to read csv: %s", err)
	}

	// set up algorithm runner
	calcs := calculators.New()
	runner := algorithm.New(zipData, calcs)

	// set up db
	opt := option.WithCredentialsFile(os.Getenv("FIREBASE_CREDENTIAL_PATH"))
	dbClient, err := db.NewClient(ctx, opt)
	if err != nil {
		log.Fatalf("Failed to set up querier: %s", err)
	}
	log.Println("created dbclient")
	querier := db.New(dbClient)

	var (
		matchService = service.New(runner, querier)
	)

	matchService.RunMatchAlgorithm(ctx)
}
